package big_task.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

public class TextFile {

    public static String read(final String path) {
        String result = null;
        try (BufferedReader r =
                     Files.newBufferedReader(Paths.get(path), Charset.defaultCharset())) {
            result = r.lines().collect(Collectors.joining(""));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
