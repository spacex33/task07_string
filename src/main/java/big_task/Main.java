package big_task;

import big_task.io.TextFile;
import big_task.model.Text;
import big_task.util.TextUtil;

public class Main {

    private Main() {
    }

    public static void main(final String[] args) {
        String textDocument = TextFile.read("harry_potter_2_chapter1.txt");
        Text text = new Text(textDocument);

        System.out.println(TextUtil.findLargestPalindromicSubstring(text));
    }
}
