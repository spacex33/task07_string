package big_task.util;

public class RegexConstant {

    private RegexConstant() {
    }

    public static final String VOWELS = "[aeiouAEIOU]";
    public static final String NOT_VOWELS = "[^aeiouAEIOU]";
    public static final String NO_SYMBOLS = "[^\\w\\s]";
    public static final String STARTS_WITH_VOWEL = "^[aieouAIEOU].*";
    public static final String STARTS_WITH_CONSONANT =
            "^[bcdfghjklmnpqrstvxzwyBCDFGHJKLMNPQRSTVXZWY].*";
}
