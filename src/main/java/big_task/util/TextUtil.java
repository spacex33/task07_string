package big_task.util;

import big_task.model.Sentence;
import big_task.model.Text;
import big_task.model.Word;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TextUtil {

    private TextUtil() {
    }

    /**
     * Task 1.
     *
     * @return the largest number of sentences of the text,
     * which contain the following words.
     */
    public static int sentencesWithSameWordsAmount(final Text text, final List<Word> words) {
        return (int) text.getSentences().stream()
                .filter(e -> e.getWords().containsAll(words))
                .count();
    }

    /**
     * Task 2.
     *
     * @return sentences of the given text in ascending order of words in each of them.
     */
    public static List<Sentence> sortSentencesByWordAmount(Text text) {
        return text.getSentences().stream()
                .sorted(Comparator.comparingInt(e -> e.getWords().size()))
                .collect(Collectors.toList());
    }

    /**
     * Task 3.
     *
     * @return a word in the first sentence, which is not in any of the other sentences.
     */
    public static Optional<Word> getUniqueWordFromFirstSentence(final Text text) {
        Word word = null;
        // iterate by words in first sentence
        for (Word firstSentenceWord : text.getSentences().get(0).getWords()) {
            // get a list of words from a sublist from the second to the last sentence,
            // and then try to find our word, if it is not found, save it in a local variable
            if (!text.getWords(text.getSentences().subList(1, text.getSentences().size()))
                    .contains(firstSentenceWord)) {
                word = firstSentenceWord;
            }
        }
        return Optional.ofNullable(word);
    }

    /**
     * Task 4.
     *
     * @return all words of the text find
     * and print without repeating a word of a given length.
     */
    public static List<Word> getWordsByLengthFromInterrogativeSentences(final Text text,
                                                                        final int length) {
        // get a list of interrogative sentences
        List<Sentence> sentences = text.getSentences().stream()
                .filter(sentence -> sentence.getString().contains("?"))
                .collect(Collectors.toList());

        // convert to a word list and find a word of appropriate length
        return text.getWords(sentences).stream()
                .filter(word -> word.getString().length() == length)
                .distinct()
                .collect(Collectors.toList());
    }

    /**
     * Task 5.
     *
     * @return text in which each sentence of text to swap the first word
     * beginning with a vowel letter longest word.
     */
    public static Text replaceVowelStartedWordByLongestWordInSentence(final Text text) {
        for (Sentence sentence : text.getSentences()) {

            try {
                Word firstVowelStartedWord = sentence.getWords().stream()
                        .filter(word -> word
                                .getString()
                                .matches(RegexConstant.STARTS_WITH_VOWEL))
                        .findFirst()
                        .orElseThrow(NoSuchElementException::new);
                int firstVowelStartedWordListIndex = sentence
                        .getWords()
                        .indexOf(firstVowelStartedWord);

                Word longestWord = sentence.getWords().stream()
                        .max(Comparator.comparingInt(value -> value.getString().length()))
                        .get();
                int longestWordListIndex = sentence.getWords().indexOf(longestWord);

                sentence.getWords().set(firstVowelStartedWordListIndex, longestWord);
                sentence.getWords().set(longestWordListIndex, firstVowelStartedWord);

            } catch (NoSuchElementException e) {
                // there is no word beginning in a vowel in this sentence,
                // so let's skip this sentence
            }
        }
        return text;
    }

    /**
     * Task 6.
     * <p>
     * Prints words in alphabetical order in the first letter.
     * Words starting with a new letter are typed
     * with a paragraph indentation.
     */
    public static void printWordsSortedInAlphabeticalOrder(final Text text) {
        LinkedHashMap<Character, List<Word>> words = text.getWords().stream()
                .sorted((word1, word2) -> word1
                        .getString()
                        .compareToIgnoreCase(word2.getString()))
                .distinct()
                .collect(Collectors.groupingBy(
                        word -> word.getString().charAt(0),
                        LinkedHashMap::new,
                        Collectors.toList()));

        for (Character character : words.keySet()) {
            System.out.println(words.get(character));
        }
    }

    /**
     * Task 7.
     *
     * @return sorted words of the text by increasing the percentage of vowels.
     */
    public static List<Word> sortWordsByVowelsPercent(final Text text) {
        return text.getWords().stream()
                .distinct()
                .sorted(Comparator.comparingInt(Word::getVowelsPercent))
                .collect(Collectors.toList());
    }

    /**
     * Task 8.
     *
     * @return words of the text, beginning with vowels, which
     * are sorted alphabetically by the first consonant letter of the word.
     */
    public static List<Word> sortWordsByConsonantsInAlphabeticalOrder(final Text text) {
        return text.getWords().stream()
                .filter(word -> word.getString().matches(RegexConstant.STARTS_WITH_VOWEL))
                .distinct()
                .sorted((word1, word2) -> word1.getString().replaceAll(RegexConstant.VOWELS, "")
                        .compareToIgnoreCase(word2.getString().replaceAll(RegexConstant.VOWELS, "")))
                .collect(Collectors.toList());
    }

    /**
     * Task 9.
     *
     * @return words which are sorted by the number of words in the given letter.
     * Words with the same number placed in alphabetical order.
     */
    public static List<Word> sortWordsByAmountOfCharacterInWord(final Text text,
                                                                final String character) {
        return text.getWords().stream()
                .sorted((word1, word2) -> {

                    int amountOfCharacterInWord1 = word1.getString().length()
                            - word1.getString().replace(character, "").length();
                    int amountOfCharacterInWord2 = word2.getString().length()
                            - word2.getString().replace(character, "").length();

                    if (amountOfCharacterInWord1 == amountOfCharacterInWord2) {
                        return word1.getString().compareToIgnoreCase(word2.getString());
                    } else if (amountOfCharacterInWord1 > amountOfCharacterInWord2) {
                        return 1;
                    } else {
                        return -1;
                    }
                })
                .collect(Collectors.toList());
    }

    /**
     * Task 10.
     *
     * @return each word from a text, in which is found how many times it occurs in each sentence,
     * and sorted words by decreasing the total number of occurrences.
     */
    public static List<Word> getSortedByFrequencyWords(final Text text) {
        List<Word> words = text.getWords().stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .sorted(Comparator.comparingInt(value -> value.getValue().intValue()))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        Collections.reverse(words);
        return words;
    }

    /**
     * Task 11.
     *
     * @return each sentence of the text, in which removed the substring of the maximum length
     * that begins and ends with the given characters.
     */
    public static List<Sentence> removeSubstringOfMaxLength(final Text text,
                                                            final String character1,
                                                            final String character2) {
        return text.getSentences().stream()
                .map(sentence -> new Sentence(sentence.getString()
                        .replaceAll(TextUtil.getSentenceWithRemovedSubstring(
                                sentence, character1, character2).getString(), "")))
                .collect(Collectors.toList());
    }

    private static Sentence getSentenceWithRemovedSubstring(final Sentence sentence,
                                                            final String character1,
                                                            final String character2) {
        int index = sentence.getString().indexOf(character1);
        int firstInd = sentence.getString().indexOf(character1, index);
        int lastInd = sentence.getString().indexOf(character2, index);

        List<String> substrings = new ArrayList<>();

        while ((firstInd < lastInd) && (firstInd != -1 && lastInd != -1)) {
            substrings.add(sentence.getString().substring(firstInd, lastInd + 1));
            index = lastInd;
            firstInd = sentence.getString().indexOf(character1, index);
            lastInd = sentence.getString().indexOf(character2, index);
        }

        if (substrings.isEmpty()) {
            return sentence;
        } else {
            String maxLengthSubstring =
                    Collections.max(substrings, Comparator.comparingInt(String::length));
            sentence.setString(maxLengthSubstring);
            return sentence;
        }
    }

    /**
     * Task 12.
     *
     * @return list of words, in which removed words of a given length,
     * beginning with a consonant letter.
     */
    public static List<Word> removeWordsStartedWithConsonant(final Text text,
                                                             final int length) {
        return text.getWords().stream()
                .filter(e -> e.getString().length() == length)
                .filter(word -> word.getString().matches(RegexConstant.STARTS_WITH_CONSONANT))
                .collect(Collectors.toList());
    }

    /**
     * Task 13.
     *
     * @return sorted words in the text by decreasing the number of occurrences of a given character,
     * and in the case of equality - by alphabet.
     */
    public static List<Word> sortWordsByNumberOfOccurrencesOfGivenCharacter(final Text text,
                                                                            final String character) {
        return text.getWords().stream()
                .distinct()
                .sorted((word1, word2) -> {
                    int charOccurrenceWord1 = word1.getString().length()
                            - word1.getString().replace(character, "").length();
                    int charOccurrenceWord2 = word2.getString().length()
                            - word2.getString().replace(character, "").length();

                    if (charOccurrenceWord1 == charOccurrenceWord2) {
                        return word1.getString().compareToIgnoreCase(word2.getString());
                    } else if (charOccurrenceWord1 > charOccurrenceWord2) {
                        return -1;
                    } else {
                        return 1;
                    }
                })
                .collect(Collectors.toList());
    }

    /**
     * Task 14.
     *
     * @return a substring of maximum length, which is a palindrome,
     * that is, read to the left to right and from the right to the left of the same.
     */
    public static String findLargestPalindromicSubstring(final Text text) {
        String str = text.getString();

        if (str.length() == 1) {
            return str;
        }
        String longest = str.substring(0, 1);
        for (int i = 0; i < str.length(); i++) {

            String tmp = findFromCenter(str, i, i);
            if (tmp.length() > longest.length()) {
                longest = tmp;
            }
            tmp = findFromCenter(str, i, i + 1);
            if (tmp.length() > longest.length()) {
                longest = tmp;
            }
        }
        return longest;
    }

    private static String findFromCenter(String str, int begin, int end) {
        while (begin >= 0 && end <= str.length() - 1 && str.charAt(begin) == str.charAt(end)) {
            begin--;
            end++;
        }
        return str.substring(begin + 1, end);
    }

    /**
     * Task 15.
     *
     * @return every word in the text by deleting all subsequent (previous)
     * occurrences of the first (last) letters of the word.
     */
    public static List<Word> removeFromStringAllCharsThatAreFirstAndLastCharsInString(final Text text) {
        // I know, this task could be done in a more elegant way with regular exceptions,
        // I will rewrite this code later.. maybe

        List<Word> words = new ArrayList<>();
        for (Word word : text.getWords()) {
            if (word.getString().length() < 3) {
                words.add(word);
                continue;
            }
            String newWord = word.getString().charAt(0)
                    + word.getString()
                    .substring(1, word.getString().length() - 1)
                    .replaceAll(Character.toString(word.getString().charAt(0)), "")
                    .replaceAll(Character.toString(word.getString().charAt(word.getString().length() - 1)), "")
                    + word.getString().charAt(word.getString().length() - 1);

            words.add(new Word(newWord));
        }
        return words;
    }

    /**
     * Task 16.
     *
     * @return list of the words, which is replaced
     * with a specified substring of a given length,
     * whose length may not match the length of the word.
     */
    public static List<Word> replaceSentenceWordsBySubstrings(final Sentence sentence,
                                                              final int wordLength,
                                                              final String subString) {
        return sentence.getWords().stream()
                .map(word -> {
                    if (word.getString().length() == wordLength) {
                        return new Word(subString);
                    } else {
                        return word;
                    }
                })
                .collect(Collectors.toList());
    }
}
