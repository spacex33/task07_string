package big_task.model;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class Text {

    private static final String[] ABBREVIATIONS = {
            "Dr.", "Prof.", "Mr.", "Mrs.", "Ms.", "Jr.", "Ph.D."
    };

    private String text;
    private List<Word> words;
    private List<Sentence> sentences;

    public Text(final String text) {
        this.text = text;
        this.sentences = splitIntoSentences(text);
        this.words = getWords(this.sentences);
    }

    public static List<Sentence> splitIntoSentences(final String document) {
        List<String> sentenceList = new ArrayList<>();

        BreakIterator bi = BreakIterator.getSentenceInstance(Locale.US);
        bi.setText(document);

        int start = bi.first();
        int end = bi.next();
        int tempStart = start;

        while (end != BreakIterator.DONE) {
            String sentence = document.substring(start, end);
            if (!hasAbbreviation(sentence)) {
                sentence = document.substring(tempStart, end);
                tempStart = end;
                sentenceList.add(sentence);
            }
            start = end;
            end = bi.next();
        }
        return sentenceList
                .stream()
                .map(Sentence::new)
                .collect(Collectors.toList());
    }

    private static boolean hasAbbreviation(final String sentence) {
        if (sentence == null || sentence.isEmpty()) {
            return false;
        }
        for (String w : ABBREVIATIONS) {
            if (sentence.contains(w)) {
                return true;
            }
        }
        return false;
    }

    public String getString() {
        return text;
    }

    public List<Word> getWords(final List<Sentence> sentences) {
        List<Word> wordList = new ArrayList<>();
        for (Sentence sentence : sentences) {
            wordList.addAll(sentence.getWords());
        }
        return wordList;
    }

    public List<Word> getWords() {
        return words;
    }

    public List<Sentence> getSentences() {
        return sentences;
    }

    @Override
    public String toString() {
        return "" + sentences;
    }
}
