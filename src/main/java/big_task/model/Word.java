package big_task.model;

import big_task.util.RegexConstant;

public class Word {

    private String word;
    private static final int PERCENT = 100;

    public Word(final String word) {
        this.word = word;
    }

    public int getVowelsPercent() {
        return word.replaceAll(RegexConstant.NOT_VOWELS, "").length()
                * PERCENT
                / word.length();
    }

    public String getString() {
        return word;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Word receivedWord = (Word) o;
        return word != null
                ? word.equals(receivedWord.getString())
                : receivedWord.getString() == null;
    }

    @Override
    public int hashCode() {
        return word != null
                ? word.hashCode()
                : 0;
    }

    @Override
    public String toString() {
        return "Word{"
                + "word='"
                + word
                + '\''
                + '}';
    }
}
