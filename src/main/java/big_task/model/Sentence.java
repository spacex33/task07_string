package big_task.model;

import big_task.util.RegexConstant;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Sentence {

    private String sentence;
    private List<Word> words;

    public Sentence(final String sentence) {
        this.sentence = sentence;
        this.words = splitIntoWords(sentence);
    }

    private List<Word> splitIntoWords(final String text) {
        String[] strings = text
                .replaceAll(RegexConstant.NO_SYMBOLS, "")
                .toLowerCase()
                .split(" ");
        return Arrays.stream(strings)
                .map(Word::new)
                .filter(word -> word.getString().length() > 0)
                .collect(Collectors.toList());
    }

    public void setWords(final List<Word> words) {
        this.words = words;
    }

    public String getString() {
        return sentence;
    }

    public void setString(final String sentence) {
        this.sentence = sentence;
    }

    public List<Word> getWords() {
        return words;
    }

    @Override
    public String toString() {
        return "" + sentence + "\n";
    }
}
